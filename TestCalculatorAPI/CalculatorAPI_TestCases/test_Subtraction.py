import pytest

from CalculatorAPI.CalculatorAPI import Calculator

@pytest.mark.usefixtures("CreateObjectForUeInTestClass")
class TestSubtractionFunction():
    Cal=None
    def testSubtractionWithPositiveNumbers(self):
        #Cal=Calculator()
        Res = TestSubtractionFunction.Cal.Subtraction(100, 20)
        assert Res == 80

    def testSubtractionWithBigNumbers(self):
        #Cal = Calculator()
        Res = TestSubtractionFunction.Cal.Subtraction   (10000, 2000)
        assert Res == 8000


