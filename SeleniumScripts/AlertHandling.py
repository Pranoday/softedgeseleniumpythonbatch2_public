from selenium import webdriver
import time

driver = webdriver.Chrome(executable_path="D:\\SoftedgeSeleniumPythonBatch_2\\Drivers\\chromedriver.exe")
driver.get("https://courses.letskodeit.com/practice")
NameField=driver.find_element_by_name("enter-name")
NameField.send_keys("Pranoday")
EnteredName=NameField.get_attribute("value")
#CSS syntax:TagName[AttributeName='AttributeValue']
AlertBtn=driver.find_element_by_css_selector("input[value='Alert']")
AlertBtn.click()
Al=driver.switch_to.alert
AlertText=Al.text
#if(AlertText=="Please enter valid username")

if(EnteredName in AlertText):
    print("Correct value is shown in Alert text..PASSED")
else:
    print("Correct value is NOT shown in Alert text..FAILED")
Al.accept()