from selenium import webdriver
import time

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import ActionChains
from selenium.webdriver.support.select import Select
#We can not use Select class for Dropdown implemented using other than Select/Option tags
driver = webdriver.Chrome(executable_path="D:\\SoftedgeSeleniumPythonBatch_2\\Drivers\\chromedriver.exe")
driver.get("https://courses.letskodeit.com/practice")
HideButton=driver.find_element_by_id("hide-textbox")
HideButton.click()
time.sleep(5)
TextBox=driver.find_element_by_id("displayed-text")
IsDisplayed=TextBox.is_displayed()
if(IsDisplayed==False):
    print("Hide button worked fine....PASSED")
else:
    print("Hide button did not work fine....FAILED")

ShowButton=driver.find_element_by_id("show-textbox")
ShowButton.click()
time.sleep(5)
IsDisplayed=TextBox.is_displayed()
if(IsDisplayed==True):
    print("Show button worked fine....PASSED")
else:
    print("Show button did not work fine....FAILED")

driver.close()
