from selenium import webdriver
import time

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import ActionChains
from selenium.webdriver.support.select import Select
#We can not use Select class for Dropdown implemented using other than Select/Option tags
driver = webdriver.Chrome(executable_path="D:\\SoftedgeSeleniumPythonBatch_2\\Drivers\\chromedriver.exe")
driver.get("https://courses.letskodeit.com/practice")
BmwCheckbox=driver.find_element_by_id("bmwcheck")
BmwCheckbox.click()
time.sleep(5)
if(BmwCheckbox.is_selected()==True):
    print("BMW checkbox got selected after clicking..PASSED")
else:
    print("BMW checkbox did not get selected after clicking..FAILED")
BmwCheckbox.click()
time.sleep(5)
if(BmwCheckbox.is_selected()==False):
    print("BMW checkbox got Deselected after clicking again..PASSED")
else:
    print("BMW checkbox did not get Deselected after clicking again..FAILED")
driver.close()