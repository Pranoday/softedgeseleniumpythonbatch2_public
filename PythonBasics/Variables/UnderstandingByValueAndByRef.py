lst1=[1,"Pranoday",10.0,False]
'''
Here we are not copying the contents of lst1 in another variable called lst2
We are copying the Memory Address,so lst1 and lst2 will point to the same Memory Location
Changes done using 1 variable can also be seen if we use another variable

'''
lst2=lst1
print("List 1 : ",lst1)
print("List 2 : ",lst2)
lst1[0]=1000
print("Changed content of the list",lst1)
print("Changed content of the list",lst2)
lst1=[1,"Pranoday",10.0,False]
lst3=lst1.copy()
print("List 1 : ",lst1)
print("List 3 : ",lst3)
lst3[0]=10000
print("Modified content of the list1",lst1)
print("Mofified content of the list3",lst3)
