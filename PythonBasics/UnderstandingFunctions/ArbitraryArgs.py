'''
    Function called with arbitrary arguments,accept the sent arguments in
    tuple
'''

def Addition(*Numbers):
    print(Numbers)          #(10, 20, 30)
    print(*Numbers)         #10 20 30
    sum=0
    for num in Numbers:
        sum=sum+num
    return sum

print(Addition(10,20,30))
print(Addition(10,20))
print(Addition(10,20,30,40))