Lst=[10,20,30,40]
print("Accessing list members using for loop")
for l in Lst:
    print(l)

Name="Pranoday"

print("Printing string using for loop")
for ch in Name:
    print(ch,end='')

print("Printing numbers using for loop and range function")
Numbers=range(1,11)
for num in Numbers:
    print(num)


