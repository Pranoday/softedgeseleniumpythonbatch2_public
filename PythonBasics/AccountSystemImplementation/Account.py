from abc import abstractmethod, ABC
from typing import final

from multipledispatch import dispatch

from AccountSystemImplementation.InsufficientBalanaceException import InsuffiencentBalanceException


class Account(ABC):
    INTERESTRATE=10.0

    '''
    def __init__(self,ActNo,Bal):
        self._AccountNo=ActNo
        self._Balance=Bal

    '''
    def __init__(self,*Args):
        if(len(Args)>2):
            raise("Account object can only be creared either by sending 1 or 2 arguments")
        elif(len(Args)==2):
            self._AccountNo=Args[0]
            self._Balance = Args[1]
        else:
            self._AccountNo = Args[0]
            self._Balance = 0.0
    #Method Overloading
    @dispatch(int)
    def DepositeAmount(self,Amnt):
        #This method asks for a money to be deposited
        self._Balance=self._Balance+Amnt

    @dispatch()
    def DepositeAmount(self):
        # This method does not ask for a money to be deposited.In this case
        #it takes 1000 as default
        self._Balance=self._Balance+1000


    def WithdrawAmount(self,Amnt):
        '''if(self.Balance>=Amnt):
            self.Balance = self.Balance - Amnt
        else:
            print("No sufficient balance available")
       '''
        if (self._Balance<Amnt):
            raise InsuffiencentBalanceException()
        self._Balance = self._Balance - Amnt
    def GetBalance(self):
        return self._Balance

    def GetAccountNo(self):
        return self._AccountNo

    def EarnInterest(self):
        InterestEarn=(self._Balance*Account.INTERESTRATE)/100
        self._Balance=self._Balance+InterestEarn

    @abstractmethod
    def OpenAccount(self):
        pass

    @final
    def CloseAccount(self):
        print("Submit application to close account")
        print("Withdraw whole money")
        print("Closing will take 2 days")

