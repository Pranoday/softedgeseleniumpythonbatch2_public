from AccountSystemImplementation.Account import Account
from AccountSystemImplementation.InsufficientBalanaceException import InsuffiencentBalanceException


class SavingsAccount(Account):

    def __init__(self,ActNo,Bal):
        super().__init__(ActNo,Bal)

    # Overriding is done
    def WithdrawAmount(self, Amnt):
            AmountLeft = self._Balance - Amnt

            if (AmountLeft < 1000 and self._Balance <= Amnt):
                raise InsuffiencentBalanceException()
            self._Balance = self._Balance - Amnt

    def OpenAccount(self):
        print("Submit KYC docs")
        print("KYC docs will be scrutinized")
        print("And then account will be opened, which may take 8 working days")